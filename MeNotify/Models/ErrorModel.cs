﻿#region using
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
#endregion
namespace MeNotify.Models
{
    /// <summary>
    /// Error model class
    /// </summary>
    public class ErrorModel
    {
        /// <summary>
        /// LogId of each error
        /// </summary>
        public int LogId { get; set; }
        /// <summary>
        /// indicates the user who generated the error
        /// </summary>
        public string ChangeByUser { get; set; }
        /// <summary>
        /// indicates when the error was generated
        /// </summary>
        public DateTime ChangeByDate { get; set; }
        /// <summary>
        /// Category specifies the tyoe of error
        /// </summary>
        public string Category { get; set; }
        /// <summary>
        /// Contains error description
        /// </summary>
        public string LogMsg { get; set; }
    }

}