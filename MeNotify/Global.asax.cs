﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace MeNotify
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        //string connectionstr = "Server=f80c9d2a-0725-4272-b139-a3df0076e35b.sqlserver.sequelizer.com;Database=dbf80c9d2a07254272b139a3df0076e35b;User ID=tlczthzdfpiufmgu;Password=fHfLEVSdiXmwe3fyfx3cumVSQph7AoWpesXtUeaaSbU6fFctE4RWHnA2Uv4pMUK4;";
        readonly string connectionstr = "Server=e8125e04-bf67-4d94-ba4e-a3e5013008e0.sqlserver.sequelizer.com;Database=dbe8125e04bf674d94ba4ea3e5013008e0;User ID=czzwayaqsctcakde;Password=PVzRYRQDXBjXT6KdSJsdo6j6wZe3tEky5wysTedwnxeFWm3UCSwyhc4RKBt8MVkr;";
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            AuthConfig.RegisterAuth();
            SqlDependency.Start(connectionstr);
            Bootstrapper.Initialise();
            //Server = f80c9d2a - 0725 - 4272 - b139 - a3df0076e35b.sqlserver.sequelizer.com; Database = dbf80c9d2a07254272b139a3df0076e35b; User ID = tlczthzdfpiufmgu; Password = fHfLEVSdiXmwe3fyfx3cumVSQph7AoWpesXtUeaaSbU6fFctE4RWHnA2Uv4pMUK4;
        }
        protected void Application_End()
        {
            //Stop SQL dependency
            SqlDependency.Stop(connectionstr);
        }
    }
}