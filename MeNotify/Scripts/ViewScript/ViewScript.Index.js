﻿var Page = $(function () {
    
    function init() {
        // Declare a proxy to reference the hub.
        var Errornotifications = $.connection.errorMessageHub;
        // Create a function that the hub can call to broadcast messages.
        Errornotifications.client.updateErrorMessages = function () {
            getAllErrors();

        };
        // Start the connection.
        $.connection.hub.start().done(function () {
            
            var data = getAllErrors();

        }).fail(function (e) {
            alert(e);
        });

    }


    //Method to display the charts.
    function displayCharts(webService, windowService, web) {
        var s1 = [];
        var s2 = [];
        var s3 = [];
        var ticks = [];

        var webServiceErrorCount = 0;
        var windowServiceErrorCount = 0;
        var webUIErrorsCount = 0;

        for (var tick in webService) {
            ticks.push(tick);
        }
        for (var key in webService) {
            if (webService.hasOwnProperty(key)) { // this will check if key is owned by data object and not by any of it's ancestors
                s1.push(webService[key]); // this will show each key with it's value
            }
        }
        for (var key in windowService) {
            if (windowService.hasOwnProperty(key)) { // this will check if key is owned by data object and not by any of it's ancestors
                s2.push(windowService[key]); // this will show each key with it's value
            }
        }
        for (var key in web) {
            if (web.hasOwnProperty(key)) { // this will check if key is owned by data object and not by any of it's ancestors
                s3.push(web[key]); // this will show each key with it's value
            }
        }
        // Can specify a custom tick Array.
        // Ticks should match up one for each y value (category) in the series.
        var plot1 = $.jqplot('chartBar', [s1, s2, s3], {
            height: 200,
            width: 300,
            // The "seriesDefaults" option is an options object that will
            // be applied to all series in the chart.
            seriesDefaults: {
                renderer: $.jqplot.BarRenderer,
                rendererOptions: { fillToZero: true }
            },
            // Custom labels for the series are specified with the "label"
            // option on the series option.  Here a series option object
            // is specified for each series.
            series: [
				{ label: 'WebService Errors' },
				{ label: 'Windows Service Errors' },
				{ label: 'UI Errors' }
            ],
            // Show the legend and put it outside the grid, but inside the
            // plot container, shrinking the grid to accomodate the legend.
            // A value of "outside" would not shrink the grid and allow
            // the legend to overflow the container.
            legend: {
                show: true,
                placement: 'outsideGrid'
            },
            axes: {
                // Use a category axis on the x axis and use our custom ticks.
                xaxis: {
                    renderer: $.jqplot.CategoryAxisRenderer,
                    ticks: ticks
                },
                // Pad the y axis just a little so bars can get close to, but
                // not touch, the grid boundaries.  1.2 is the default padding.
                yaxis: {

                    tickOptions: { formatString: '%d' }
                }
            }
        });

        for (var key in s1) {
            webServiceErrorCount += s1[key];
        }
        for (var key in s2) {
            windowServiceErrorCount += s2[key];
        }
        for (var key in s3) {
            webUIErrorsCount += s3[key];
        }
        var pieChartData = [webUIErrorsCount, windowServiceErrorCount, webServiceErrorCount];

        var plot2 = $.jqplot('chartPie', [[['Web Service Errors', webServiceErrorCount], ['Windows Service Errors', windowServiceErrorCount], ['Web UI Errors', webUIErrorsCount]]],
			{
			    seriesDefaults: {
			        renderer: jQuery.jqplot.PieRenderer,
			        rendererOptions: {
			            padding: 2,
			            sliceMargin: 2,
			            showDataLabels: true
			        }
			    },
			    legend: { show: true, location: 'e' }
			});
        $("#tabs").tabs({
            activate: function (event, ui) {
                if (ui.index === 2 && plot1._drawCount === 0 && plot2._drawCount === 0) {
                    plot1.replot();

                }
            }
        });
    }
    //Method to display Grid data.
    function displayGrid(result) {
        $("#tblJQGridWebService").jqGrid(

		{
		    data: result.webServiceErrors,
		    datatype: "local",
		    rowList: [10, 20, 30],
		    colNames: ['LogId', 'LogMsg', 'ChangeByUser', 'ChangeByDate', 'Category'],
		    colModel: [
			{ name: 'LogId', index: 'LogId', width: 150, sorttype: "int" },
			{ name: 'LogMsg', index: 'LogMsg', width: 300, sortable: false },
			 { name: 'ChangeByUser', index: 'ChangeByUser', width: 150, sortable: false },
			{ name: 'ChangeByDate', index: 'ChangeByDate', width: 100, sortable: false, formatter: "date" },
			 { name: 'Category', index: 'Category', width: 150, sortable: false }],
		    rowNum: 10,
		    height: 400,
		    viewrecords: true,
		    sortorder: "desc",
		    caption: "Web Service Errors",
		    scrollOffset: 0,
		    pager: "#plistWebService"
		});


        $("#tblJQGridWindowsService").jqGrid(

		{
		    data: result.WindowsServiceErrors,
		    datatype: "local",
		    rowList: [10, 20, 30],
		    colNames: ['LogId', 'LogMsg', 'ChangeByUser', 'ChangeByDate', 'Category'],
		    colModel: [
			{ name: 'LogId', index: 'LogId', width: 150, sorttype: "int" },
			{ name: 'LogMsg', index: 'LogMsg', width: 300, sortable: false },
			 { name: 'ChangeByUser', index: 'ChangeByUser', width: 150, sortable: false },
			{ name: 'ChangeByDate', index: 'ChangeByDate', width: 100, sortable: false, formatter: "date" },
			 { name: 'Category', index: 'Category', width: 150, sortable: false }],
		    rowNum: 10,
		    height: 400,
		    viewrecords: true,
		    sortorder: "desc",
		    caption: "Windows Service Errors",
		    scrollOffset: 0,
		    pager: "#plistWindowsService"
		});

        $("#tblJQGridWeb").jqGrid(
		{
		    data: result.WebUIErrors,
		    datatype: "local",
		    rowList: [10, 20, 30],
		    colNames: ['LogId', 'LogMsg', 'ChangeByUser', 'ChangeByDate', 'Category'],
		    colModel: [
			{ name: 'LogId', index: 'LogId', width: 150, sorttype: "int" },
			{ name: 'LogMsg', index: 'LogMsg', width: 300, sortable: false },
			 { name: 'ChangeByUser', index: 'ChangeByUser', width: 150, sortable: false },
			{ name: 'ChangeByDate', index: 'ChangeByDate', width: 100, sortable: false, formatter: "date" },
			 { name: 'Category', index: 'Category', width: 150, sortable: false }],
		    rowNum: 10,
		    height: 400,
		    viewrecords: true,
		    sortorder: "desc",
		    caption: "Web Errors",
		    scrollOffset: 0,
		    pager: "#plistWeb"
		});
    }


    //Method to get all the error data.
    function getAllErrors() {
        $.ajax({
            url: '/home/getmessages',
            contentType: 'application/json ; charset:utf-8',
            type: 'GET',
            dataType: 'json'
        }).success(function (result) {
            /* Todo : add face book like indicator
            if (result.errors[0].Category === "WebService") {
                WebServiceErrorsCnt++;
                if (isFirstTime) {
                    var cntWeb = parseInt($("#divWebService").html());
                    $("#divWebService").html(++cntWeb);
                    isFirstTime = false;
                }
            }
            else if (result.errors[0].Category === "WindowsService") {
                WindowsServiceErrorsCnt++;
                if (isFirstTime) {
                    var cntWind = parseInt($("#divWindows").html());
                    $("#divWindows").html(++cntWind);
                    isFirstTime = false;
                }
            }
            else if (result.errors[0].Category === "WebApplication") {
                WebErrorsCnt++;
                if (isFirstTime) {
                    var cntUI = parseInt($("#divWebUI").html());
                    $("#divWebUI").html(++cntUI);
                    isFirstTime = false;
                }
            }*/
            displayCharts(result.WeekErrorDataWebService, result.WeekErrorDataWindowsService, result.WeekErrorDataWeb);
            displayGrid(result);
            $('#tblJQGridWebService').jqGrid('setGridParam', { datatype: 'local', data: result.WebServiceErrors }).trigger('reloadGrid');
            $('#tblJQGridWindowsService').jqGrid('setGridParam', { datatype: 'local', data: result.WindowsServiceErrors }).trigger('reloadGrid');
            $('#tblJQGridWeb').jqGrid('setGridParam', { datatype: 'local', data: result.WebUIErrors }).trigger('reloadGrid');
            $("#webUI").empty().html(result.UIErrorCount);
            $("#windowsService").empty().html(result.WinServiceCount);
            $("#webService").empty().html(result.WebserErrorCount);






        }).error(function (error) {
            alert(error);
        });
    }

    return {
        Init: init()
    };
    Page.Init();
});